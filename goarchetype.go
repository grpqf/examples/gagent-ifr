package main

import (
	"util/command"
	"util/structure"

	"flag"
	"util/git"
	"util/yaml"
)

type ServiceUser struct {
	UserName string `yaml:"username"`
	Path     string `yaml:"path"`
}

func main() {
	repository_vars := flag.String("repository_vars", "", "Informe o nome do repositorio de vars")
	branch_vars := flag.String("branch_vars", "master", "Informe o nome do branch para o repositório de vars")
	branch_gtw := flag.String("branch_gtw", "master", "Informe o nome do branch para o repositório de gtw")
	user_repo := flag.String("user_repo", "", "Informe o usuario para o repositorio")
	pass_repo := flag.String("pass_repo", "", "Informe a senha para o repositorio")
	aws_key := flag.String("aws_key", "", "Informe a senha para o repositorio")
	aws_sec := flag.String("aws_sec", "", "Informe a senha para o repositorio")
	name_repo := flag.String("name_repo", "", "Informe o nome do repositório (role que será validada ou acionada)")
	docker_user := flag.String("docker_user", "", "Informe o nome do usuario para o docker")
	docker_pass := flag.String("docker_pass", "", "Informe a senha do usuario para o docker")
	image_version := flag.String("image_version", "", "Informe a versão para a imagem")
	target := flag.String("target", "", "Informe o target")
	jenkins_url := flag.String("jenkins_url", "", "Informe a url para o jenkins")
	path_mgnt := flag.String("path_mgnt", "/opt/tmp", "Path")
	token_spotinst := flag.String("token_spotinst", "", "Informe o token da spotinst")
	spotinst_account := flag.String("spotinst_account", "", "Informe a conta do spotinst")
	bucket_repo_name := flag.String("bucket_repo_name", "", "Informe o nome do repositório para o bucket")
	kms_id := flag.String("kms_id", "", "Informe o kms_id")

	flag.Parse()

	var pathPlb string

	pathPlb = git.CreateStructure(*path_mgnt)

	git.Clone("archetype-ansible_gtw", *branch_gtw, pathPlb, *user_repo, *pass_repo)

	if *repository_vars != "" {
		git.Clone(*repository_vars, *branch_vars, pathPlb, *user_repo, *pass_repo)
		structure.CopyFile(pathPlb+"/."+*repository_vars+"/vars.yml", pathPlb+"/.archetype-ansible_gtw/vars.yml")
	} else {
		structure.CopyFile("vars.yml", pathPlb+"/.archetype-ansible_gtw/vars.yml")
	}

	extra_vars := " user_repo=" + *user_repo + " pass_repo=" + *pass_repo + " name_repo=" + *name_repo +
		" aws_key=" + *aws_key + " aws_sec=" + *aws_sec + " docker_user=" + *docker_user + " docker_pass=" + *docker_pass +
		" image_version=" + *image_version + " target=" + *target + " jenkins_url=" + *jenkins_url +
		" token_spotinst=" + *token_spotinst + " spotinst_account=" + *spotinst_account + " bucket_repo_name=" + *bucket_repo_name +
		" kms_id=" + *kms_id

	command.Exec("ansible-playbook", "-i", pathPlb+"/.archetype-ansible_gtw/inventory", pathPlb+"/.archetype-ansible_gtw/requirements.yml", "--extra-vars="+extra_vars)

	serviceUser := ServiceUser{UserName: structure.GenerateRandomUsr(20), Path: structure.GenerateRandomPath(*path_mgnt)}

	path := structure.CreateServiceDirectory(pathPlb)

	pathusers := yaml.GenerateYamlToFile(path, &serviceUser)

	command.Exec(pathPlb, "ansible-playbook", "-i", pathPlb+"/.archetype-ansible_gtw/inventory", pathPlb+"/.archetype-ansible_gtw/main.yml", "-e", "pathusers="+pathusers+extra_vars)

	structure.CleanAll(pathPlb)
}
